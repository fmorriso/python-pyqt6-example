import pyautogui


class GuiSettings:

    def __init__(self, pct: float = 0.5, multiple: int = 10):
        """Initialize the screen's settings."""
        # Screen settings

        # calculate game size as a percentage of device screen size
        device_width, device_height = pyautogui.size()
        self.screenPct: float = pct
        multiple_of: int = multiple
        self.scaled_width: int = int((device_width * self.screenPct // multiple_of) * multiple_of)
        self.scaled_height: int = int((device_height * self.screenPct // multiple_of) * multiple_of)
        print(f'scaled width={self.scaled_width}, height={self.scaled_height}')

        self.scaleFactor = device_width / device_height + self.screenPct
        # print(f'scale factor = {self.scaleFactor}')

        self.bg_color = (230, 0, 230)
