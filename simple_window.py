# from PyQt6.QtWidgets import QMainWindow, QApplication, QPushButton
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *
from gui_settings import GuiSettings


class SimpleWindow(QMainWindow):
    def __init__(self, title):
        # super().__init__()
        super(SimpleWindow, self).__init__()

        self.settings = GuiSettings(0.45, 100)
        self.setMinimumSize(self.settings.scaled_width, self.settings.scaled_height)

        self.setWindowTitle(f"Hello World using python {title}")

        button = QPushButton("My simple app.")
        button.pressed.connect(self.close)

        self.setCentralWidget(button)
        self.show()

  