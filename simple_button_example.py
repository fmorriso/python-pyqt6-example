# from PyQt6.QtWidgets import QMainWindow, QApplication, QPushButton
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *

from output_utilities import OutputUtils
from gui_settings import GuiSettings


class SimpleButtonExample(QMainWindow):
    def __init__(self, title):
        # super().__init__()
        super(SimpleButtonExample, self).__init__()

        self.settings = GuiSettings(0.45, 100)
        self.setMinimumSize(self.settings.scaled_width, self.settings.scaled_height)
        self.setWindowTitle(f"Simple Button Example {title}")
        self.title = title

        self.button_is_checked = True

        self.button = QPushButton("Press Me!")
        """
        self.button.setCheckable(True)        
        self.button.released.connect(self.the_button_was_released)
        self.button.setChecked(self.button_is_checked)
        """
        self.button.clicked.connect(self.the_button_was_clicked)

        # Set the central widget of the Window.
        self.setCentralWidget(self.button)

        self.show()

    def the_button_was_clicked(self):
        OutputUtils.display_message("Button clicked", "ths_button_was_clicked", self)

    def the_button_was_toggled(self, checked):
        self.button_is_checked = checked
        print(f'{self.button_is_checked=}')
        OutputUtils.display_message(f'Checked? {self.button_is_checked}', 'the_button_was_toggled', self)

    def the_button_was_released(self):
        self.button_is_checked = self.button.isChecked()
        msg = f'{self.button_is_checked=}'
        print(msg)
        OutputUtils.display_message(msg, 'the_button_was_released', self)

    def the_button_was_clicked(self):
        msg = "You already clicked me."
        self.button.setText(msg)
        self.button.setEnabled(False)
        OutputUtils.display_message(msg, 'the_button_was_clicked', self)

        # Also change the window title.
        self.setWindowTitle(f"My Oneshot App - using python version {self.title}")