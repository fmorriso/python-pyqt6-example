import sys

from PyQt6.QtWidgets import *

from simple_button_example import SimpleButtonExample
# from PyQt6.QtCore import *

from simple_window import SimpleWindow


def get_python_version() -> str:
    return f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"


def create_and_display_simple_window():
    app = QApplication(sys.argv)
    w: SimpleWindow = SimpleWindow(get_python_version())
    app.exec()


def create_and_display_simple_button_window():
    app = QApplication(sys.argv)
    w: SimpleButtonExample = SimpleButtonExample(get_python_version())
    app.exec()


if __name__ == '__main__':
    print(f'Python version: {get_python_version()}')
    # create_and_display_simple_window()
    create_and_display_simple_button_window()
